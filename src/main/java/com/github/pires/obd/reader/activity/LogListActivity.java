package com.github.pires.obd.reader.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.util.Log;


import com.github.pires.obd.reader.R;
import com.google.inject.Inject;

import java.io.File;
import java.util.ArrayList;

import roboguice.activity.RoboActivity;
import roboguice.RoboGuice;
import roboguice.inject.InjectPreference;


import static com.github.pires.obd.reader.activity.ConfirmDialog.createDialog;
import static java.lang.System.out;
//reuses tripslist layout

public class LogListActivity
        extends RoboActivity
        implements ConfirmDialog.Listener {

    @Inject
    private SharedPreferences prefs;
    //private SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
    //nur in on create sonst error



    //private List<TripRecord> records;
    ArrayList<File> FilesInDirectory = new ArrayList<File>();

    //private TripLog triplog = null;

    //private TripListAdapter adapter = null;
    private ArrayAdapter arrayAdapter;


     //       Log.d("TAG", "Stopping live data..");

    //= new ArrayAdapter<String>();
    private File LoggingDirectory;

    /// the currently selected row from the list of records
    private int selectedRow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trips_list);
        File sdCard = Environment.getExternalStorageDirectory();

        LoggingDirectory = new File(sdCard.getAbsolutePath() + File.separator +prefs.getString(ConfigActivity.DIRECTORY_FULL_LOGGING_KEY,
                getString(R.string.default_dirname_full_logging)));



        //prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Log.d("bug", LoggingDirectory.getName());
        Log.d("bug", LoggingDirectory.getAbsolutePath());
        if(LoggingDirectory.canRead()){
            Log.d("bug", "true");
        }




        ListView lv = (ListView) findViewById(R.id.tripList);

        //triplog = TripLog.getInstance(this.getApplicationContext());
        //records = triplog.readAllRecords();
        //adapter = new TripListAdapter(this, records);

        //FilesInDirectory = GetFiles(LoggingDirectory.getPath());

        for (int i=0; i<LoggingDirectory.listFiles().length; i++)
            FilesInDirectory.add(LoggingDirectory.listFiles()[i]);



        arrayAdapter = new ArrayAdapter<File>(this,
                android.R.layout.simple_list_item_1, FilesInDirectory);


        lv.setAdapter(arrayAdapter);
        registerForContextMenu(lv);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_trips_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        // create the menu
        getMenuInflater().inflate(R.menu.context_trip_list, menu);

        // get index of currently selected row
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        selectedRow = (int) info.id;

        // get record that is currently selected
        //TripRecord record = records.get(selectedRow);!!!!

        File selectedFile = FilesInDirectory.get(selectedRow);
    }

    /*private void deleteTrip() {
        // get the record to delete from our list of records
        TripRecord record = records.get(selectedRow);

        // attempt to remove the record from the log
        if (triplog.deleteTrip(record.getID())) {

            // remove the record from our list of records
            records.remove(selectedRow);

            // update the list view
            adapter.notifyDataSetChanged();
        } else {
            //Utilities.toast(this,getString(R.string.toast_delete_failed));
        }
    }*/

    public boolean onContextItemSelected(MenuItem item) {

        // get index of currently selected row
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        selectedRow = (int) info.id;

        switch (item.getItemId()) {
            case R.id.itemDelete:
                showDialog(ConfirmDialog.DIALOG_CONFIRM_DELETE_ID);
                return true;

            default:
                return super.onContextItemSelected(item);
        }
    }

    protected Dialog onCreateDialog(int id) {
        return createDialog(id, this, this);
    }

    /**
     * DESCRIPTION:
     * Called when the user has selected a gasoline record to delete
     * from the log and has confirmed deletion.
     */
    /*protected void deleteRow() {

        // get the record to delete from our list of records
        TripRecord record = records.get(selectedRow);

        // attempt to remove the record from the log
        if (triplog.deleteTrip(record.getID())) {
            records.remove(selectedRow);
            adapter.notifyDataSetChanged();
        } else {
            //Utilities.toast(this,getString(R.string.toast_delete_failed));
        }
    }*/

    @Override
    public void onConfirmationDialogResponse(int id, boolean confirmed) {
        removeDialog(id);
        if (!confirmed) return;

        switch (id) {
            case ConfirmDialog.DIALOG_CONFIRM_DELETE_ID:
               // deleteRow();
                break;

            default:
                //Utilities.toast(this,"Invalid dialog id.");
        }

    }
}
