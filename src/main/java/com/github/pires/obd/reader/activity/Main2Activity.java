package com.github.pires.obd.reader.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.pires.obd.reader.R;

import roboguice.activity.RoboActivity;

public class Main2Activity extends RoboActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
    }
}
