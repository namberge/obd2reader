package com.github.pires.obd.reader.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.pires.obd.reader.R;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.view.LineChartView;

public class ScreenSlidePageFragment5testjanek extends Fragment {

    private LineChartView lineChartView;
    private ViewGroup rootView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_screen_slide_page5, container, false);
        drawSinAbsChart();
        return rootView;
    }


    public void drawSinAbsChart() {
        String decimalPattern = "#.##";
        DecimalFormat decimalFormat = new DecimalFormat(decimalPattern);

        lineChartView = (LineChartView)rootView.findViewById(R.id.chart);//getactivity has to be used in a fragment

        List<PointValue> values = new ArrayList<PointValue>();

        PointValue tempPointValue;
        for (float i = 0; i <= 360.0; i+= 15.0f) {
            tempPointValue = new PointValue(i, Math.abs((float)Math.sin(Math.toRadians(i))));
            tempPointValue.setLabel(decimalFormat
                    .format(Math.abs((float)Math.sin(Math.toRadians(i)))));
            values.add(tempPointValue);
        }

        Line line = new Line(values)
                .setColor(Color.BLUE)
                .setCubic(false)
                .setHasPoints(true).setHasLabels(true);
        List<Line> lines = new ArrayList<Line>();
        lines.add(line);

        LineChartData data = new LineChartData();
        data.setLines(lines);

        List<AxisValue> axisValuesForX = new ArrayList<>();
        List<AxisValue> axisValuesForY = new ArrayList<>();
        AxisValue tempAxisValue;
        for (float i = 0; i <= 360.0f; i += 30.0f){
            tempAxisValue = new AxisValue(i);
            tempAxisValue.setLabel(i+"\u00b0");
            axisValuesForX.add(tempAxisValue);
        }

        for (float i = 0.0f; i <= 1.00f; i += 0.25f){
            tempAxisValue = new AxisValue(i);
            tempAxisValue.setLabel(""+i);
            axisValuesForY.add(tempAxisValue);
        }

        Axis xAxis = new Axis(axisValuesForX);
        Axis yAxis = new Axis(axisValuesForY);
        data.setAxisXBottom(xAxis);
        data.setAxisYLeft(yAxis);


        lineChartView.setLineChartData(data);


    }
}
